package com.yourbooks.bookstore.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.yourbooks.bookstore.dao.BookDAO;
import com.yourbooks.bookstore.dao.OrderDAO;
import com.yourbooks.bookstore.dao.UserDAO;
import com.yourbooks.bookstore.dao.impl.BookDAOImpl;
import com.yourbooks.bookstore.dao.impl.OrderDAOImpl;
import com.yourbooks.bookstore.dao.impl.UserDAOImpl;

@Configuration	
@ComponentScan(basePackages="com.yourbooks.bookstore")
@EnableWebMvc
public class MvcConfiguration extends WebMvcConfigurerAdapter {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources");
	}
	
	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/books_database");
		dataSource.setUsername("root");
		dataSource.setPassword("***");
		
		return dataSource;
	}
	
	@Bean
	public BookDAO getBookDAO() {
		return new BookDAOImpl(getDataSource());
	}
	
	@Bean
	public UserDAO getUserDAO() {
		return new UserDAOImpl(getDataSource());
	}
	
	@Bean
	public OrderDAO getOrderDAO() {
		return new OrderDAOImpl(getDataSource());
	}
	
}

package com.yourbooks.bookstore.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yourbooks.bookstore.dao.UserDAO;
import com.yourbooks.bookstore.domain.User;
import com.yourbooks.bookstore.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDao;

	@Override
	public User getUser(String login) {
		
		return userDao.getUser(login);
	}

	@Override
	public List<User> userList() {
		
		return userDao.userList();
	}

	@Override
	public void registerNewUser(User user) {

		userDao.registerNewUser(user);
		
	}
	
	@Override
	public void updateUser(User user) {
		userDao.updateUser(user);
	}
	
	
}

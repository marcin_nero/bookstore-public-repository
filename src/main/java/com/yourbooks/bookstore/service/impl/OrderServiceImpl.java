package com.yourbooks.bookstore.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yourbooks.bookstore.dao.OrderDAO;
import com.yourbooks.bookstore.domain.Book;
import com.yourbooks.bookstore.domain.User;
import com.yourbooks.bookstore.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDAO orderDAO;
	
	@Override
	public void createOrder(User user) {
		// TODO Auto-generated method stub
		orderDAO.createOrder(user);
		
	}

	@Override
	public void cancelOrder(String order_id) {
		// TODO Auto-generated method stub
		orderDAO.cancelOrder(order_id);
	}

	@Override
	public void addBookToOrder(String book_id, String order_id) {
		// TODO Auto-generated method stub
		orderDAO.addBookToOrder(book_id, order_id);
	}

	@Override
	public List<Book> orderedBookList(String order_id) {
		// TODO Auto-generated method stub
		return orderDAO.orderedBookList(order_id);
	}

	@Override
	public void deleteBookFromOrder(String book_id, String order_id) {
		// TODO Auto-generated method stub
		orderDAO.deleteBookFromOrder(book_id, order_id);		
	}

	@Override
	public void finalizeOrder(String order_id) {
		// TODO Auto-generated method stub
		orderDAO.finalizeOrder(order_id);
	}
	
	@Override
	public boolean doesUserHasActiveOrder(User user) {
		return orderDAO.doesUserHasActiveOrder(user);
	}

	@Override
	public String getActiveOrderId(User user) {
		// TODO Auto-generated method stub
		return orderDAO.getActiveOrderId(user);
	}

	@Override
	public BigDecimal countOrderValue(String order_id) {
		// TODO Auto-generated method stub
		return orderDAO.countOrderValue(order_id);
	}

}

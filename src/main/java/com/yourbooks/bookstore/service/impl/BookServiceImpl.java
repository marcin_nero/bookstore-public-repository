package com.yourbooks.bookstore.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yourbooks.bookstore.dao.BookDAO;
import com.yourbooks.bookstore.domain.Book;
import com.yourbooks.bookstore.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDAO bookDao;

	@Override
	public Book findBookById(String id) {

		return bookDao.findBookById(id);
	}

	@Override
	public List<Book> list() {

		return bookDao.list();
	}

	@Override
	public List<Book> booksByCategory(String category) {

		return bookDao.booksByCategory(category);
	}

	@Override
	public void insertBook(Book book) {
		
		bookDao.insertBook(book);
	}
	
	@Override
	public void updateBook(Book updateBook) {
		bookDao.updateBook(updateBook);
	}

	@Override
	public List<Book> listOf(String title) {
		return bookDao.listOf(title);
	}

	@Override
	public List<Book> search(String query) {
		
		return bookDao.search(query);
	}

}

package com.yourbooks.bookstore.service;

import java.util.List;

import com.yourbooks.bookstore.domain.Book;

public interface BookService {
	
	public Book findBookById(String id);
	
	public List<Book> list();
	
	public List<Book> booksByCategory(String category);
	
	public void insertBook(Book book);
	
	public void updateBook(Book updateBook);
	
	public List<Book> listOf(String title);
	
	public List<Book> search(String query);

}

package com.yourbooks.bookstore.service;

import java.math.BigDecimal;
import java.util.List;

import com.yourbooks.bookstore.domain.Book;
import com.yourbooks.bookstore.domain.User;

public interface OrderService {
	public void createOrder(User user);
	public void cancelOrder(String order_id);
	public void addBookToOrder(String book_id, String order_id);
	public List<Book> orderedBookList(String order_id);
	public void deleteBookFromOrder(String book_id, String order_id);
	public void finalizeOrder(String order_id);
	public boolean doesUserHasActiveOrder(User user);
	public String getActiveOrderId(User user);
	public BigDecimal countOrderValue(String order_id);
}

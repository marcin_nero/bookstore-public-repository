package com.yourbooks.bookstore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yourbooks.bookstore.domain.Book;
import com.yourbooks.bookstore.domain.User;
import com.yourbooks.bookstore.service.BookService;
import com.yourbooks.bookstore.service.OrderService;
import com.yourbooks.bookstore.service.UserService;

@Controller
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private UserService userService;
	
	@RequestMapping("/order")
	public String orderDetail(@RequestParam("order_id") String order_id, Model model) {
		
		// pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		
		if(user != null) {
			List<Book> orderList = orderService.orderedBookList(order_id);
			
			model.addAttribute("loggedPersonName", user.getName());
			model.addAttribute("orderList", orderList);
			model.addAttribute("total", orderService.countOrderValue(order_id));
			model.addAttribute("order_id", orderService.getActiveOrderId(user));
		}else {
			model.addAttribute("loggedPersonName", name);
		}
		
		return "order";
	}

	@RequestMapping("/newOrder")
	public String newOrder(Model model) {

		// pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		
		if(user != null) {
			
			// sprawdzenie czy Klient ma jaki� otwarty koszyk, je�li tak to przekierowanie do widoku
			if(orderService.doesUserHasActiveOrder(user)) {
				return "redirect:/order/order?order_id=" + orderService.getActiveOrderId(user);
			}
			orderService.createOrder(user);
			orderService.countOrderValue(orderService.getActiveOrderId(user));
			//model.addAttribute("order_id", orderService.getActiveOrderId(user));
		}else {
			model.addAttribute("loggedPersonName", name);
			return "/order";
		}
		return "redirect:/order/order?order_id=" + orderService.getActiveOrderId(user); // zrobi� przekierowanie do widoku zam�wienia
	}
	
	@RequestMapping("/cancelOrder")
	public String cancelOrder(@RequestParam("order_id") String order_id) {
		
		orderService.cancelOrder(order_id);
		
		return "redirect:/";
	}
	
	@RequestMapping("addBook")
	public String addBookToOrder(@RequestParam("book_id") String book_id) {
		
		// pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		
		if(user != null) {
			
			//sprawdzenie czy jest otwarty koszyk
			if(orderService.doesUserHasActiveOrder(user) == false) {
				orderService.createOrder(user);
			}
			
			orderService.addBookToOrder(book_id, orderService.getActiveOrderId(user));
			orderService.countOrderValue(orderService.getActiveOrderId(user));
			
		}else {
			return "redirect:/login";
		}
		
		return "redirect:/order/order?order_id=" + orderService.getActiveOrderId(user);
	}
	
	@RequestMapping("/delete")
	public  String deleteBookFromOrder(@RequestParam("book_id") String book_id) {
		// pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		
		if(user != null) {
			
			//sprawdzenie czy jest otwarty koszyk
			if(orderService.doesUserHasActiveOrder(user)) {
				orderService.deleteBookFromOrder(book_id, orderService.getActiveOrderId(user));
				orderService.countOrderValue(orderService.getActiveOrderId(user));
			}else {
				return "redirect:/login";
			}
			
		}
		
		return "redirect:/order/order?order_id=" + orderService.getActiveOrderId(user);
	}
	
	@RequestMapping("/finalizeOrder")
	public String finalizeOrder(@RequestParam("order_id") String order_id, Model model) {
		
		// pobranie imienia zalogowanej osoby
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				String name = auth.getName();
				User user = userService.getUser(name);
				
				if(user != null) {
					
					//sprawdzenie czy jest otwarty koszyk
					if(orderService.doesUserHasActiveOrder(user)) {
						orderService.finalizeOrder(order_id);
						model.addAttribute("success", "Twoje zam�wienie o numerze "+ order_id + " jest w trakcie realizacji");
						model.addAttribute("loggedPersonName", user.getName());
					}else {
						return "redirect:/login";
					}
					
				}
		
		return "finalizeOrder";
	}
	
}

package com.yourbooks.bookstore.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.yourbooks.bookstore.domain.Book;
import com.yourbooks.bookstore.domain.User;
import com.yourbooks.bookstore.service.BookService;
import com.yourbooks.bookstore.service.OrderService;
import com.yourbooks.bookstore.service.UserService;

@Controller
@RequestMapping("/")
public class BookController {

	@Autowired
	private BookService bookService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private UserService userService;

	@RequestMapping
	public ModelAndView listBooks(ModelAndView model) throws IOException {
		
		//fantasy slider
		List<Book> fantasyBooks = bookService.booksByCategory("fantasy");
		model.addObject("fantasy", fantasyBooks);
		
		//action slider
		List<Book> actionBooks = bookService.booksByCategory("action");
		model.addObject("action", actionBooks);
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		if(user != null) {
			
			if(orderService.doesUserHasActiveOrder(user)) {
				model.addObject("order", orderService.getActiveOrderId(user));
			}
			
			model.addObject("loggedPersonName", user.getName());
		}else {
			model.addObject("loggedPersonName", name);
		}
		
		model.setViewName("home");

		return model;
	}

	@RequestMapping("/byCategory/{category}")
	public String booksByCategory(Model model, @PathVariable("category") String category) {

		List<Book> listBooksByCategory = bookService.booksByCategory(category);

		model.addAttribute("books", listBooksByCategory);
		
		//pobranie imienia zalogowanej osoby oraz id koszyka
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		if(user != null) {
			model.addAttribute("loggedPersonName", user.getName());
		
			if(orderService.doesUserHasActiveOrder(user)) {
				model.addAttribute("order", orderService.getActiveOrderId(user));
			}
		
		}else {
			model.addAttribute("loggedPersonName", name);
		}

		return "books";
	}

	@RequestMapping("/book")
	public String bookById(Model model, @RequestParam("id") String id) {

		Book book = new Book();

		book = bookService.findBookById(id);

		model.addAttribute("book", book);
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		if(user != null) {
			model.addAttribute("loggedPersonName", user.getName());
		
			if(orderService.doesUserHasActiveOrder(user)) {
				model.addAttribute("order", orderService.getActiveOrderId(user));
			}
		
		}else {
			model.addAttribute("loggedPersonName", name);
		}
		return "book";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String getAddNewBookForm(Model model) {
		Book newBook = new Book();
		model.addAttribute("newBook", newBook);
		model.addAttribute("greeting", "Dodaj now� ksi��k� do naszej wspania�ej ksi�garni");
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		if(user != null) {
			model.addAttribute("loggedPersonName", user.getName());
			
			if(orderService.doesUserHasActiveOrder(user)) {
				model.addAttribute("order", orderService.getActiveOrderId(user));
			}
		}else {
			model.addAttribute("loggedPersonName", name);
		}

		return "addBook";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String processAddNewBookForm(@ModelAttribute("newBook") @Valid Book newBook, BindingResult result) {		
		
		if(result.hasErrors()) {
			return "addBook";
		}
		
		bookService.insertBook(newBook);
		return "redirect:/menu";
	}
	
	@RequestMapping(value="/update/", method = RequestMethod.GET)
	public String updateBookForm(Model model, @RequestParam("id") String bookId) {
 		Book updateBook = bookService.findBookById(bookId);
		model.addAttribute("updateBook", updateBook);
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		if(user != null) {
			model.addAttribute("loggedPersonName", user.getName());
			
			if(orderService.doesUserHasActiveOrder(user)) {
				model.addAttribute("order", orderService.getActiveOrderId(user));
			}
		}else {
			model.addAttribute("loggedPersonName", name);
		}
		
		return "updateBook";
	}
	
	@RequestMapping(value = "/update/", method = RequestMethod.POST)
	public String processUpdateBookForm(@ModelAttribute("updateBook") @Valid Book updateBook, BindingResult result) {
		
		if(result.hasErrors()) {
			return "menu";
		}
		
		bookService.updateBook(updateBook);

		return "redirect:/menu";
	}
	
	@RequestMapping(value = "/special/{title}")
	public String testSite(Model model, @PathVariable("title") String title) {
		
		List<Book> lista = bookService.listOf(title);
		model.addAttribute("books", lista);
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		if(user != null) {
			model.addAttribute("loggedPersonName", user.getName());
			
			if(orderService.doesUserHasActiveOrder(user)) {
				model.addAttribute("order", orderService.getActiveOrderId(user));
			}
		}else {
			model.addAttribute("loggedPersonName", name);
		}
		
		return "books";
	}
	
	@RequestMapping(value="/search_", method = RequestMethod.GET)
	public String search(Model model, @RequestParam("query") String query) {
		
		List<Book> lista = bookService.search(query);
		model.addAttribute("books", lista);
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.getUser(name);
		if(user != null) {
			model.addAttribute("loggedPersonName", user.getName());
			
			if(orderService.doesUserHasActiveOrder(user)) {
				model.addAttribute("order", orderService.getActiveOrderId(user));
			}
		}else {
			model.addAttribute("loggedPersonName", name);
		}
		
		return "books";
	}

}

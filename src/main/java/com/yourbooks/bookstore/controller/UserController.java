package com.yourbooks.bookstore.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.yourbooks.bookstore.domain.User;
import com.yourbooks.bookstore.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/allUsers")
	public String usersList(Model model) {
		
		model.addAttribute("users", userService.userList());
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		model.addAttribute("loggedPersonName", name);
		
		return "userList";
	}
	
	@RequestMapping("/{login}")
	public String getUserProfile(@PathVariable("login") String login, Model model) {
		
		model.addAttribute("user", userService.getUser(login));
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		model.addAttribute("loggedPersonName", name);
		
		return "userProfile";
	}
	
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public String getRegisterNewUserForm(Model model) {
		User user = new User();
		model.addAttribute("newUser", user);

		return "register";
	}
	
	@RequestMapping(value="/register", method = RequestMethod.POST)
	public String processRegisterNewUser(@ModelAttribute("newUser") @Valid User newUser, BindingResult result) {
		if(result.hasErrors()) {
			return "register";
		}
		
		userService.registerNewUser(newUser);
		return "redirect:/";
	}
	

	
	
	@RequestMapping(value="/update/{login}", method = RequestMethod.GET)
	public String updateUserForm(Model model, @PathVariable("login") String login ) {
		User userToUpdate = userService.getUser(login);
		model.addAttribute("updateUser", userToUpdate);
		
		//pobranie imienia zalogowanej osoby
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		if(name.length()>0) {
		model.addAttribute("loggedPersonName", name);
		}
		
		return "updateUser";
	}
	
	@RequestMapping(value="/update/{login}", method = RequestMethod.POST)
	public String processUpdateUserForm(@ModelAttribute("updateUser") @Valid User updateUser, BindingResult result ) {
		
		if(result.hasErrors()) {
			return "redirect:/menu";
		}
		
		userService.updateUser(updateUser);
		
		return "redirect:/user/{login}";
	}
	
}

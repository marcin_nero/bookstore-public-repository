package com.yourbooks.bookstore.domain;

import java.math.BigDecimal;
import java.util.Date;

public class Order {
	
	private int order_id;
	private int user_id;
	private Date order_date;
	private BigDecimal order_value;
	private boolean isOrderActive;
	
	public int getOrder_id() {
		return order_id;
	}
	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public Date getOrder_date() {
		return order_date;
	}
	public void setOrder_date(Date order_date) {
		this.order_date = order_date;
	}
	public BigDecimal getOrder_value() {
		return order_value;
	}
	public void setOrder_value(BigDecimal order_value) {
		this.order_value = order_value;
	}
	public boolean getIsOrderActive() {
		return isOrderActive;
	}
	public void setIsOrderActive(boolean isOrderActive) {
		this.isOrderActive = isOrderActive;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + order_id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (order_id != other.order_id)
			return false;
		return true;
	}
}

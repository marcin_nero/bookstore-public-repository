package com.yourbooks.bookstore.domain;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

public class User {
	
	private int user_id;
	
	@Size(min=4,max=20, message="{Size.User.login.validation}")
	private String login;
	
	@Size(min=5, max=20, message="{Size.User.password.validation}")
	private String password;
	
	@Size(max=20, message="{Size.User.name.validation}")
	private String name;
	
	@Size(max=20, message="{Size.User.lastname.validation}")
	private String lastName;
	
	@Size(min=3, max=50, message="{Size.User.email.validation}")
	@Email(message="{Email.User.email.validation}")
	private String email;
	
	private String role;
	private String address;
	
	public User() {
		
	}
	
	public User(int id, String login, String password) {
		this.user_id = id;
		this.login = login;
		this.password = password;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
}

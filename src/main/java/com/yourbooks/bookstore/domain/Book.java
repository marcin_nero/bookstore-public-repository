package com.yourbooks.bookstore.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

public class Book {
	
	@Size(min = 1, max = 20, message = "{Size.Book.category.validation}")
	private String category;//
	
	@Size(min=1, max=60, message="{Size.Book.title.validation}")
	private String title;//
	
	@Min(value=0, message="{Min.Book.price.validation}")
	@Digits(integer=4, fraction=2, message="{Digits.Book.price.validation}")
	private BigDecimal price;//
	
	@Size(min=1, max=30, message = "{Size.Book.author.validation}")
	private String author;//
	
	@Size(min = 1, max=250, message = "{Size.Book.publisher.validation}")
	private String publisher;//
	
	@Size(min = 1, max = 20, message = "{Size.Book.language.validation}")
	private String language;//
	
	@Size(min = 13, max = 13, message = "{Size.Book.isbn.validation}")
	private String ISBN;//
	
	@Min(value = 1, message = "{Min.Book.unitsInStock.validation}")
	private long unitsInStock;
	
	private long unitsInOrder;
	
	@Pattern(regexp="B[0-9]+", message="{Pattern.Book.id.validation}")
	@Size(min=5, max=5, message="{Size.Book.id.sizeValidation}")
	private String productId;//
	
	@Size(min = 1, max = 1000, message = "{Size.Book.review.validation}")
	private String review;//
	
	private boolean	bestseller;
	private boolean comingSoon;
	private boolean newInStore;
	private boolean	outlet;
	
	
	public Book(){
		super();
	}
	
	public Book(String category, String title, String author ) {
		this.category = category;
		this.title = title;
		this.author = author;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public long getUnitsInStock() {
		return unitsInStock;
	}
	public void setUnitsInStock(long unitsInStock) {
		this.unitsInStock = unitsInStock;
	}
	public long getUnitsInOrder() {
		return unitsInOrder;
	}
	public void setUnitsInOrder(long unitsInOrder) {
		this.unitsInOrder = unitsInOrder;
	}
	public String getId() {
		return productId;
	}
	public void setId(String id) {
		this.productId = id;
	}
	
	public void setReview(String review) {
		this.review = review;
	}
	
	public String getReview() {
		return review;
	}
	
	public void setBestseller(boolean bestseller) {
		this.bestseller = bestseller;
	}
	
	public boolean getBestseller() {
		return bestseller;
	}
	
	public void setComingSoon(boolean comingSoon) {
		this.comingSoon = comingSoon;
	}
	
	public boolean getComingSoon() {
		return comingSoon;
	}
	
	public void setNewInStore(boolean newInStore) {
		this.newInStore = newInStore;
	}
	
	public boolean getNewInStore() {
		return newInStore;
	}
	
	public void setOutlet(boolean outlet) {
		this.outlet = outlet;
	}
	
	public boolean getOutlet() {
		return outlet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		int val = 0;
		String str="N/A";
		try {
			val = Integer.parseInt(getId());
		}catch (NumberFormatException e){
			
		}
		
		result = prime * result + val;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (productId != other.productId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Product [id=" + productId + ", category=" + category + ", title=" + title + ", author=" + author + "]";
	}
	
	

}

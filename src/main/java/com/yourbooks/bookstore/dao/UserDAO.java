package com.yourbooks.bookstore.dao;

import java.util.List;

import com.yourbooks.bookstore.domain.User;


public interface UserDAO {
	public User getUser(String login);
	public List<User> userList();
	public void registerNewUser(User user);
	public void updateUser(User user);
}

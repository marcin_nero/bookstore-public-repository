package com.yourbooks.bookstore.dao;

import java.util.List;

import com.yourbooks.bookstore.domain.Book;

public interface BookDAO {
	public void insertBook(Book book);
	public Book findBookById(String id);
	public List<Book> list();
	public List<Book> booksByCategory(String category);
	public void updateBook(Book book);
	public List<Book> listOf(String title);
	public List<Book> search(String query);
}

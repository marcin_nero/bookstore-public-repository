package com.yourbooks.bookstore.dao.impl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.yourbooks.bookstore.dao.BookDAO;
import com.yourbooks.bookstore.domain.Book;

public class BookDAOImpl implements BookDAO {

	private JdbcTemplate jdbcTemplate;

	public BookDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void insertBook(Book book) {
		String sql = "INSERT INTO book (category, title, author, bookId, price, review, publisher, language, ISBN, unitsInStock) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		jdbcTemplate.update(sql, book.getCategory(), book.getTitle(), book.getAuthor(), book.getId(), book.getPrice(),
				book.getReview(), book.getPublisher(), book.getLanguage(), book.getISBN(), book.getUnitsInStock());
	}

	@Override
	public Book findBookById(String id) {
		String sql = "SELECT * FROM book WHERE bookId=\'" + id + "\'";
		return jdbcTemplate.query(sql, new ResultSetExtractor<Book>() {

			@Override
			public Book extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					Book book = new Book();
					book.setId(rs.getString("bookId"));
					book.setCategory(rs.getString("category"));
					book.setTitle(rs.getString("title"));
					book.setAuthor(rs.getString("author"));
					book.setPrice(new BigDecimal(rs.getDouble("price")));
					book.setPublisher(rs.getString("publisher"));
					book.setLanguage(rs.getString("language"));
					book.setISBN(rs.getString("ISBN"));
					book.setUnitsInStock(rs.getInt("unitsInStock"));
					book.setReview(rs.getString("review"));
					book.setBestseller(rs.getBoolean("bestseller"));
					book.setNewInStore(rs.getBoolean("newInStore"));
					book.setComingSoon(rs.getBoolean("comingSoon"));
					book.setOutlet(rs.getBoolean("outlet"));

					return book;
				}

				return null;
			}

		});

	}

	@Override
	public List<Book> list() {
		String sql = "SELECT * FROM book";
		List<Book> listBook = jdbcTemplate.query(sql, new RowMapper<Book>() {

			@Override
			public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
				Book book = new Book();

				book.setId(rs.getString("bookId"));
				book.setCategory(rs.getString("category"));
				book.setTitle(rs.getString("title"));
				book.setAuthor(rs.getString("author"));
				book.setPrice(new BigDecimal(rs.getDouble("price")));
				book.setPublisher(rs.getString("publisher"));
				book.setLanguage(rs.getString("language"));
				book.setISBN(rs.getString("ISBN"));
				book.setUnitsInStock(rs.getInt("unitsInStock"));
				book.setReview(rs.getString("review"));
				book.setBestseller(rs.getBoolean("bestseller"));
				book.setNewInStore(rs.getBoolean("newInStore"));
				book.setComingSoon(rs.getBoolean("comingSoon"));
				book.setOutlet(rs.getBoolean("outlet"));

				return book;
			}

		});
		return listBook;
	}

	@Override
	public List<Book> booksByCategory(String category) {

		String sql = "SELECT * FROM book WHERE category = \'" + category + "\'";
		List<Book> booksByCategory = jdbcTemplate.query(sql, new RowMapper<Book>() {

			@Override
			public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
				Book book = new Book();

				book.setId(rs.getString("bookId"));
				book.setCategory(rs.getString("category"));
				book.setTitle(rs.getString("title"));
				book.setAuthor(rs.getString("author"));
				book.setPrice(new BigDecimal(rs.getDouble("price")));
				book.setPublisher(rs.getString("publisher"));
				book.setLanguage(rs.getString("language"));
				book.setISBN(rs.getString("ISBN"));
				book.setUnitsInStock(rs.getInt("unitsInStock"));
				book.setReview(rs.getString("review"));
				book.setBestseller(rs.getBoolean("bestseller"));
				book.setNewInStore(rs.getBoolean("newInStore"));
				book.setComingSoon(rs.getBoolean("comingSoon"));
				book.setOutlet(rs.getBoolean("outlet"));

				return book;
			}

		});

		return booksByCategory;
	}

	public void updateBook(Book book) {
		String sql = "UPDATE book SET title=?, author=?, category=?, price=?, publisher=?, language=?, ISBN=?, unitsInStock=?, review=?, bestseller=?, newInStore=?, comingSoon=?, outlet=? WHERE bookId=?";

		jdbcTemplate.update(sql, book.getTitle(), book.getAuthor(), book.getCategory(), book.getPrice(),
				book.getPublisher(), book.getLanguage(), book.getISBN(), book.getUnitsInStock(), book.getReview(),
				book.getBestseller(), book.getNewInStore(), book.getComingSoon(), book.getOutlet(), book.getId());
	}

	public List<Book> listOf(String title) {

		String sql = "SELECT * FROM book WHERE " + title + " = 1";
		List<Book> theList = jdbcTemplate.query(sql, new RowMapper<Book>() {

			@Override
			public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
				Book book = new Book();

				book.setId(rs.getString("bookId"));
				book.setCategory(rs.getString("category"));
				book.setTitle(rs.getString("title"));
				book.setAuthor(rs.getString("author"));
				book.setPrice(new BigDecimal(rs.getDouble("price")));
				book.setPublisher(rs.getString("publisher"));
				book.setLanguage(rs.getString("language"));
				book.setISBN(rs.getString("ISBN"));
				book.setUnitsInStock(rs.getInt("unitsInStock"));
				book.setReview(rs.getString("review"));
				book.setBestseller(rs.getBoolean("bestseller"));
				book.setNewInStore(rs.getBoolean("newInStore"));
				book.setComingSoon(rs.getBoolean("comingSoon"));
				book.setOutlet(rs.getBoolean("outlet"));

				return book;
			}

		});

		return theList;
	}

	@Override
	public List<Book> search(String query) {

		String sql = "SELECT * FROM book where title LIKE \'%" + query + "%\' OR author LIKE \'%" + query
				+ "%\' OR category LIKE \'%" + query + "%\'";
		
		List<Book> queryList = jdbcTemplate.query(sql, new RowMapper<Book>() {

			@Override
			public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
				Book book = new Book();

				book.setId(rs.getString("bookId"));
				book.setCategory(rs.getString("category"));
				book.setTitle(rs.getString("title"));
				book.setAuthor(rs.getString("author"));
				book.setPrice(new BigDecimal(rs.getDouble("price")));
				book.setPublisher(rs.getString("publisher"));
				book.setLanguage(rs.getString("language"));
				book.setISBN(rs.getString("ISBN"));
				book.setUnitsInStock(rs.getInt("unitsInStock"));
				book.setReview(rs.getString("review"));
				book.setBestseller(rs.getBoolean("bestseller"));
				book.setNewInStore(rs.getBoolean("newInStore"));
				book.setComingSoon(rs.getBoolean("comingSoon"));
				book.setOutlet(rs.getBoolean("outlet"));

				return book;
			}

		});
		
		return queryList;
	}

}

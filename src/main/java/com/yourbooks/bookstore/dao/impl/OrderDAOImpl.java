package com.yourbooks.bookstore.dao.impl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.yourbooks.bookstore.dao.OrderDAO;
import com.yourbooks.bookstore.domain.Book;
import com.yourbooks.bookstore.domain.Order;
import com.yourbooks.bookstore.domain.User;

public class OrderDAOImpl implements OrderDAO {

	private JdbcTemplate jdbcTemplate;

	public OrderDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void addBookToOrder(String book_id, String order_id) {
		// TODO Auto-generated method stub

		String sql = "INSERT INTO ordered_book (book_id, order_id) VALUES ((SELECT book.id FROM book WHERE book.bookId = ?),?)";

		jdbcTemplate.update(sql, book_id, order_id);

	}

	@Override
	public List<Book> orderedBookList(String order_id) {
		// TODO Auto-generated method stub

		String sql = "SELECT * FROM ordered_book INNER JOIN book ON ordered_book.book_id = book.id where order_id = \'" + order_id + "\'";

		List<Book> orderedBooks = jdbcTemplate.query(sql, new RowMapper<Book>() {

			@Override
			public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				Book book = new Book();

				book.setId(rs.getString("bookId"));
				book.setCategory(rs.getString("category"));
				book.setTitle(rs.getString("title"));
				book.setAuthor(rs.getString("author"));
				book.setPrice(new BigDecimal(rs.getDouble("price")));
				book.setPublisher(rs.getString("publisher"));
				book.setLanguage(rs.getString("language"));
				book.setISBN(rs.getString("ISBN"));
				book.setUnitsInStock(rs.getInt("unitsInStock"));
				book.setReview(rs.getString("review"));
				book.setBestseller(rs.getBoolean("bestseller"));
				book.setNewInStore(rs.getBoolean("newInStore"));
				book.setComingSoon(rs.getBoolean("comingSoon"));
				book.setOutlet(rs.getBoolean("outlet"));

				return book;
			}

		});

		return orderedBooks;
	}

	@Override
	public void deleteBookFromOrder(String book_id, String order_id) {
		// TODO Auto-generated method stub

		String sql = "DELETE FROM ordered_book WHERE book_id = (SELECT book.id FROM book WHERE book.bookId = ?) AND order_id= ?";

		jdbcTemplate.update(sql,book_id, order_id);

	}

	@Override
	public void createOrder(User user) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO book_order (user_id, isOrderActive) VALUES (?,?)";

		jdbcTemplate.update(sql, user.getUser_id(), true);

	}

	@Override
	public void cancelOrder(String order_id) {
		// TODO Auto-generated method stub
		String sql1 = "DELETE FROM book_order WHERE order_id = ?";

		jdbcTemplate.update(sql1, order_id);
	}

	@Override
	public void finalizeOrder(String order_id) {
		// TODO Auto-generated method stub
		Date date = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("F"); 
		
		String sql = "UPDATE book_order SET isOrderActive = false, order_date = ? WHERE order_id = ?";

		jdbcTemplate.update(sql, date, order_id);
	}
	
	@Override
	public boolean doesUserHasActiveOrder(User user) {
		
		String sql = "SELECT * FROM book_order where book_order.user_id = " + user.getUser_id();
		
		
		List<Order> ordersList = jdbcTemplate.query(sql, new RowMapper<Order>() {

			@Override
			public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				
					Order order = new Order();
					order.setOrder_id(rs.getInt("order_id"));
					order.setUser_id(rs.getInt("user_id"));
					order.setOrder_date(rs.getDate("order_date"));
					order.setOrder_value(rs.getBigDecimal("order_value"));
					order.setIsOrderActive(rs.getBoolean("isOrderActive"));
					
					return order;
			}	
		});
		
		if(ordersList.isEmpty()) {
			return false;
		}
		
		Order order = null;
		
		for(Order activeOrder : ordersList) {
			if(activeOrder.getIsOrderActive()) {
				order = activeOrder;
			}
		}
		
		if(order == null) {
			return false;
		}
		
		return order.getIsOrderActive();
	}

	@Override
	public String getActiveOrderId(User user) {
		// TODO Auto-generated method stub

		String sql = "SELECT * FROM book_order where book_order.user_id = " + user.getUser_id();
		
		List<Order> ordersList = jdbcTemplate.query(sql, new RowMapper<Order>() {

			@Override
			public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub

				Order order = new Order();
				order.setOrder_id(rs.getInt("order_id"));
				order.setUser_id(rs.getInt("user_id"));
				order.setOrder_date(rs.getDate("order_date"));
				order.setOrder_value(rs.getBigDecimal("order_value"));
				order.setIsOrderActive(rs.getBoolean("isOrderActive"));
				
				return order;
			}
		});
		
		if(ordersList.isEmpty()) {
			return "";
		}
		
		Order order = null;
		
		for(Order activeOrder : ordersList) {
			if(activeOrder.getIsOrderActive()) {
				order = activeOrder;
			}
		}
		
		if(order == null) {
			return "";
		}
		
		return "" + order.getOrder_id();
	}

	@Override
	public BigDecimal countOrderValue(String order_id) {
		// TODO Auto-generated method stub
		
		String sql = "SELECT * FROM ordered_book INNER JOIN book ON ordered_book.book_id = book.id where order_id = \'" + order_id + "\'";

		List<Book> orderedBooks = jdbcTemplate.query(sql, new RowMapper<Book>() {

			@Override
			public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				Book book = new Book();

				book.setId(rs.getString("bookId"));
				book.setCategory(rs.getString("category"));
				book.setTitle(rs.getString("title"));
				book.setAuthor(rs.getString("author"));
				book.setPrice(new BigDecimal(rs.getDouble("price")));
				book.setPublisher(rs.getString("publisher"));
				book.setLanguage(rs.getString("language"));
				book.setISBN(rs.getString("ISBN"));
				book.setUnitsInStock(rs.getInt("unitsInStock"));
				book.setReview(rs.getString("review"));
				book.setBestseller(rs.getBoolean("bestseller"));
				book.setNewInStore(rs.getBoolean("newInStore"));
				book.setComingSoon(rs.getBoolean("comingSoon"));
				book.setOutlet(rs.getBoolean("outlet"));

				return book;
			}

		});
		
		if(orderedBooks.isEmpty()) {
			String query = "UPDATE book_order SET order_value = 0 WHERE order_id =" + order_id;
			jdbcTemplate.update(query);
			return new BigDecimal(0);
		}
		
		BigDecimal  value = new BigDecimal(0);
			
		for(Book book : orderedBooks) {
			value = value.add(book.getPrice());
		}	
		
		String query = "UPDATE book_order SET order_value = " + value.doubleValue() + " WHERE order_id =" + order_id;
		jdbcTemplate.update(query);
		
		return value;
	}
}

package com.yourbooks.bookstore.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.yourbooks.bookstore.dao.UserDAO;
import com.yourbooks.bookstore.domain.User;

public class UserDAOImpl implements UserDAO {

	private JdbcTemplate jdbcTemplate;

	public UserDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public User getUser(String login) {

		String sql = "SELECT * FROM user WHERE login=\'" + login + "\'";

		return jdbcTemplate.query(sql, new ResultSetExtractor<User>() {

			@Override
			public User extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {	
					User user = new User();
					user.setUser_id(rs.getInt("user_id"));
					user.setLogin(rs.getString("login"));
					user.setPassword(rs.getString("password"));
					user.setName(rs.getString("name"));
					user.setLastName(rs.getString("lastName"));
					user.setEmail(rs.getString("email"));
					user.setRole(rs.getString("role"));
					user.setAddress(rs.getString("address"));

					return user;
				}
				return null;
			}

		});
	}

	@Override
	public List<User> userList() {

		String sql = "SELECT * FROM user";

		List<User> userList = jdbcTemplate.query(sql, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {

				User user = new User();
				user.setUser_id(rs.getInt("user_id"));
				user.setLogin(rs.getString("login"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setLastName(rs.getString("lastName"));
				user.setEmail(rs.getString("email"));
				user.setRole(rs.getString("role"));
				user.setAddress(rs.getString("address"));

				return user;
			}

		});

		return userList;
	}

	@Override
	public void registerNewUser(User user) {
		String sql = "INSERT INTO user (login, password, name, lastname, email, role) VALUES (?,?,?,?,?,?)";
		
		jdbcTemplate.update(sql, user.getLogin(), user.getPassword(), user.getName(), user.getLastName(), user.getEmail(), user.getRole());
		
		
	}
	
	@Override
	public void updateUser(User user) {
		String sql = "UPDATE user SET name=?, lastname=?, email=? WHERE login=?";
		
		jdbcTemplate.update(sql, user.getName(), user.getLastName(), user.getEmail(), user.getLogin());
	}

}

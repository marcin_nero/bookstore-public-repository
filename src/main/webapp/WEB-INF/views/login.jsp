<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>Strona logowania</title>
 	</head>
 	<body>
 		
 		<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
		
 		<c:set var="loginURL"><c:url value="login"/></c:set>
    	<div class="container">
    		<div class="row">
    			<div class="col-md-4 col-md-offset-4">
    				<div class="panel panel-dafault">
    					<div class="panel-heading text-center">
    						<h3 class="panel-title">Strona logowania</h3>
    					</div>
    					<div class="panel-body">
    						<c:if test="${not empty error }">
    							<div class="alert alert-danger">
    								<spring:message code="AbstractUserDetailsAuthenticationProvider.badCredentials" /></br>
    							</div>
    						</c:if>
    						<form action="<c:url value="/j_spring_security_check"></c:url>" method = "post">
    							<fieldset>
    								<div class="form-group">
    									<input class="form-control" placeholder="Nazwa użytkownika" name='j_username' type="text">
    								</div>
    								<div class="form-group">
    									<input class="form-control" placeholder="Hasło" name='j_password' type="password" value="">
    								</div>
    								<div>
    									<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }"/>
    								</div>
    								<div>
    									<input class="btn btn-lg btn-primary btn-block" type="submit" value="Zaloguj się">
    								</div>
    								
    								Jeśli nie posiadasz konta - <a href="<spring:url value="/user/register"></spring:url>">zarejestruj się</a>
    								
    							</fieldset>
    						</form>
    					</div>
    				</div>
    			</div>    		
    		</div>
    	</div>
    	<!-- Optional JavaScript -->
    	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	</body>
</html>
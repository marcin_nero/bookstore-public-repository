<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
	<head>
		<title></title>
 	</head>
 	<body>
    	
    	<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
		
		<!-- lista produktów -->
		
		<div class="container">
			<div class="row">
				<div class="col-md-12"></div>
				
				<div class="col-md-10">
					<c:forEach items="${books }" var="book">
						<div class="media">
							<div class="media-left media-middle" style="min-width:150px">
								<img src="<spring:url value="/resource/images/${book.id}.jpg"></spring:url>">
							</div>
							<div class="media-body">
								<div class="panel-group">
									<div class="panel panel-default" >
										<div class="panel-heading">
											<h1 class="media-heading">${book.title}</h1>
											<p>${book.author}</p>
										</div>
										<div class="panel-body pre-scrollable" style="height:200px">	
											<p>${book.review }</p>
										</div>
										<div class="panel-footer ">
											<h4><span class="label label-default">Cena: <fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2" /> zł</span>
												<c:if test="${book.bestseller }">
													<span class="label label-default">Bestseller</span>
												</c:if>
												<c:if test="${book.newInStore }">
													<span class="label label-default">Nowość</span>
												</c:if>
												<c:if test="${book.outlet }">
													<span class="label label-default">Wyprzedaż</span>
												</c:if>
											</h4>
								    		
											<p class="text-right">
												<a href="<spring:url value="/order/addBook?book_id=${book.id}"></spring:url>" class="btn btn-primary btn-large">
									    			<span class="glyphicon-shopping-cart glyphicon"></span>
									    			Dodaj do koszyka
									    		</a>
												<a href="<spring:url value="/book?id=${book.id}"></spring:url>" role="button" class="btn btn-primary">Szczegóły</a>
											</p>
										</div>	
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
				
				<div class="col-md-12"></div>
			</div>
		</div>
		
  	</body>
</html>
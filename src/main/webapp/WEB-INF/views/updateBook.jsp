<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		
		<title><spring:message code="siteTitle" /></title>
	</head>
	<body>
		
		<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
	
		<div class="container">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<form:form modelAttribute="updateBook" class="form-horizontal">
						<form:errors path="*" cssClass="alert alert-danger" element="div"/>
						<fieldset>
							<div class="panel panel-default">
								<div class="panel-header text-center">
									Zaktualizuj dane książki <span class="label label-default">${updateBook.getId()} </span>
								</div>
								<div class="panel-body">
								
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Tytuł:
											</div>
											<div class="col-md-10">
												<form:input id="title" path="title" type="text" class="form:input-large" />
												<form:errors path="title" cssClass="text-danger" />
											</div>
										</div>
									</div>	
										
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Autor:
											</div>
											<div class="col-md-10">
												<form:input id="author" path="author" type="text" class="form:input-large" />
												<form:errors path="author" cssClass="text-danger" />
											</div>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Kategoria:
											</div>
											<div class="col-md-10">
												<form:input id="category" path="category" type="text" class="form:input-large" />
												<form:errors path="category" cssClass="text-danger" />
											</div>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Cena:
											</div>
											<div class="col-md-10">
												<form:input id="price" path="price" type="number" step="0.01" class="form:input-large" />
												<form:errors path="price" cssClass="text-danger" />
											</div>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Wydawnictwo:
											</div>
											<div class="col-md-10">
												<form:input id="publisher" path="publisher" type="text" class="form:input-large" />
												<form:errors path="publisher" cssClass="text-danger" />
											</div>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Język:
											</div>
											<div class="col-md-10">
												<form:input id="language" path="language" type="text" class="form:input-large" />
												<form:errors path="language" cssClass="text-danger" />
											</div>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												ISBN:
											</div>
											<div class="col-md-10">
												<form:input id="ISBN" path="ISBN" type="text" class="form:input-large" />
												<form:errors path="ISBN" cssClass="text-danger" />
											</div>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Ilość:
											</div>
											<div class="col-md-10">
												<form:input id="unitsInStock" path="unitsInStock" type="number" min="1.00" max="100" class="form:input-large" />
												<form:errors path="unitsInStock" cssClass="text-danger" />
											</div>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Opis:
											</div>
											<div class="col-md-10">
												<form:textarea id="review" path="review"  rows="5" maxlength="1000" class="form:input-large" />
												<form:errors path="review" cssClass="text-danger" />
											</div>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Bestseller:
											</div>
											<div class="col-md-10">
												<form:checkbox path="bestseller" id="bestseller"/>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Nowości:
											</div>
											<div class="col-md-10">
												<form:checkbox path="newInStore" id="newInStore"/>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Zapowiedzi:
											</div>
											<div class="col-md-10">
												<form:checkbox path="comingSoon" id="comingSoon"/>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-2">
												Wyprzedaż:
											</div>
											<div class="col-md-10">
												<form:checkbox path="outlet" id="outlet"/>
											</div>
										</div>
									</div>
									
								</div>
								<div class="panel-footer text-center">
									<div class="form-group">
			    						<div class="col-lg-offset-2 col-lg-10">
			    							<input type="submit" id="btnAdd" class="btn btn-primary" value="Zapisz">
			    						</div>
		    						</div>
								</div>
							</div>
						</fieldset>
					</form:form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		
	</body>
</html>
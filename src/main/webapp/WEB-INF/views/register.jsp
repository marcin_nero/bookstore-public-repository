<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Rejestracja w serwisie YourBooks.com</title>
</head>
<body>

	<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
	
	<section class="container">
	    	<div class="row">
	    		<div class="col-md-8">
	    		
	    			
	    		
	    			<form:form modelAttribute="newUser" class="form-horizontal">
	    				<form:errors path="*" cssClass="alert alert-danger" element="div"/>
	    				<fieldset>
	    					<legend>Zarejestruj się</legend>
	    					
	    					
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-2" for="login">Login </label>
	    						<div class="col-lg-10">
	    							<form:input id="login" path="login" type="text" class="form:input-large"/>
	    							<form:errors path="login" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-2" for="password">Hasło </label>
	    						<div class="col-lg-10">
	    							<form:input id="password" path="password" type="password" class="form:input-large"/>
	    							<form:errors path="password" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-2" for="name">Imię </label>
	    						<div class="col-lg-10">
	    							<form:input id="name" path="name" type="text" class="form:input-large"/>
	    							<form:errors path="name" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-2" for="lastName">Nazwisko </label>
	    						<div class="col-lg-10">
	    							<form:input id="lastName" path="lastName" type="text" class="form:input-large"/>
	    							<form:errors path="lastName" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-2 for="email">email </label>
	    						<div class="col-lg-10">
	    							<form:input id="email" path="email" type="text" class="form:input-large"/>
	    							<form:errors path="email" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<div class="col-lg-10">
	    							<form:input id="role" path="role" type="hidden" value="ROLE_USER" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<div class="col-lg-offset-2 col-lg-10">
	    							<input type="submit" id="btnAdd" class="btn btn-primary" value="Zarejestruj">
	    						</div>
	    					</div>
	    					
	    				</fieldset>
	    			</form:form>
	    		</div>
	    	</div>
	    </section>
	
</body>
</html>
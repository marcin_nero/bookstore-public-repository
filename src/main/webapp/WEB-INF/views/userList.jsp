<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>Bookstore Users List</title> 	
 	</head>
	<body>
		
		<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
		
    	<section class="container">
	    	<table class="table table-dark table-hover">
	    		<thead>
	    			<tr>
	    				<th>Login</th>
	    				<th>Imię</th>
	    				<th>Nazwisko</th>
	    				<th>Email</th>
	    			</tr>
	    		</thead>
    			<tbody>
	    			<c:forEach items="${users }" var="user">	    				
		    			<tr onclick="window.location.href='/bookstore/user/${user.login}'" style="cursor.pointer">
		    					<td>${user.login }</td>
		    					<td>${user.name }</td>
		    					<td>${user.lastName }</td>
		    					<td>${user.email }</td>
		    			</tr>
	    			</c:forEach>
	    		</tbody>	
	    	</table>
    	</section>
	</body>
</html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<html>
	<head>
		<meta charset="utf-8">
 		<title>Książki</title>
 	</head>
 	<body>
	
	<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>

	    <section class="container">
	    	<div class="row">
	    		<div class="col-md-8">
	    		
	    			<p>Jesteś zalogowany jako ${loggedPersonName}.
	    		
	    			<form:form modelAttribute="newBook" class="form-horizontal">
	    				<form:errors path="*" cssClass="alert alert-danger" element="div"/>
	    				<fieldset>
	    					<legend>Dodaj nowy produkt</legend>
	    					
	    					
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="category">Kategoria </label>
	    						<div class="col-lg-10">
	    							<form:input id="category" path="category" type="text" class="form:input-large"/>
	    							<form:errors path="category" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="title">Tytuł </label>
	    						<div class="col-lg-10">
	    							<form:input id="title" path="title" type="text" class="form:input-large"/>
	    							<form:errors path="title" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="author">Autor </label>
	    						<div class="col-lg-10">
	    							<form:input id="author" path="author" type="text" class="form:input-large"/>
	    							<form:errors path="author" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="id">
									<spring:message code="addBook.form.id.label" />
								</label>
	    						<div class="col-lg-10">
	    							<form:input id="id" path="id" type="text" class="form:input-large"/>
	    							<form:errors path="id" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="price">Cena </label>
	    						<div class="col-lg-10">
	    							<form:input id="price" path="price" type="number" step="0.01" class="form:input-large"/>
	    							<form:errors path="price" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="review">Opis</label>
	    						<div class="col-lg-10">
	    							<form:input id="review" path="review" type="text" maxlength="250" class="form:input-large"/>
	    							<form:errors path="review" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="publisher">Wydawca</label>
	    						<div class="col-lg-10">
	    							<form:input id="publisher" path="publisher" type="text" class="form:input-large"/>
	    							<form:errors path="publisher" cssClass="text-danger" />
	    						</div>
	    					</div>
	    				
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="language">Język</label>
	    						<div class="col-lg-10">
	    							<form:input id="language" path="language" type="text" class="form:input-large"/>
	    							<form:errors path="language" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="ISBN">ISBN</label>
	    						<div class="col-lg-10">
	    							<form:input id="ISBN" path="ISBN" type="text" class="form:input-large"/>
	    							<form:errors path="ISBN" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="unitsInStock">Ilość</label>
	    						<div class="col-lg-10">
	    							<form:input id="unitsInStock" path="unitsInStock" type="number" step="1" class="form:input-large"/>
	    							<form:errors path="unitsInStock" cssClass="text-danger" />
	    						</div>
	    					</div>
	    					
	    					
	    					<div class="form-group">
	    						<div class="col-lg-offset-2 col-lg-10">
	    							<input type="submit" id="btnAdd" class="btn btn-primary" value="Dodaj">
	    						</div>
	    					</div>
	    					
	    				</fieldset>
	    			</form:form>
	    		</div>
	    	</div>
	    </section>
 	
 	
 	</body>
</html>
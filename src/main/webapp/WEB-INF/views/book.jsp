<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
	<head>
		<title>Strona produktu</title>
 	</head>
 	<body>
	
	<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>

    	<section class="container">
    		<div class="row">
    			<div class="col-md-5">
    				<img src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" alt="${book.title }" style="width:100%"  >
    			</div>
    			<div class="col-md-5">
    				<h1 class="text-center"><span class="label label-default">${book.title}</span></h1>
			    	<h3 class="text-danger">Cena: <fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2"  /> zł</h3>
			    	<p>Autor: ${book.author}</p>
			    	<p>Kategoria: ${book.category}</p>
			    	<p>Opis: ${book.review}</p>
			    	<p>Liczba sztuk w magazynie: ${book.unitsInStock }</p>
			    	<p>
			    		<strong>Kod produktu: </strong><span class="badge badge-primary">${book.id }</span>  
			    	</p>
			    	<p>
			    		<a href="javascript:history.back()" class="btn btn-default">
			    			<span class="glyphicons glyphicons-undo">Wstecz</span>
			    		</a>
			    		<a href="<spring:url value="/order/addBook?book_id=${book.id}"></spring:url>" class="btn btn-primary btn-large">
			    			<span class="glyphicon-shopping-cart glyphicon"></span>
			    			Dodaj do koszyka
			    		</a>
			    	</p>
    			</div>
    		</div>
    	</section>		
    </body>
</html>
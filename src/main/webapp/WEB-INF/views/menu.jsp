<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
	<head>
		<meta charset="utf-8">
		<title>Menu</title>
 	</head>
 	<body>
 		
 		<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
		
		<!-- menu admina -->
		<security:authorize access="hasRole('ROLE_ADMIN')">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="panel panel-default">
						<div class="panel-heading text-center">Menu</div>
						<div class="panel-body text-center">
							<div class="form-group">
								<a href="<spring:url value="/add"></spring:url>" class="btn btn-primary">Dodaj nowy produkt</a>
							</div>
							
							<div class="form-group">
								<form method="get" action="<spring:url value="/update/"></spring:url>" >
									<form:errors path="*" cssClass="alert alert-danger" element="div"/>
									<fieldset>
										<input type="text" name="id" value="ID produktu"><br>
										<input type="submit" id="btnAdd" class="btn btn-primary" value="Aktualizuj produkt">
									</fieldset>
								</form>						
							</div>	
							
							<div class="form-group">
								<a href="<spring:url value="/user/allUsers"></spring:url>" class="btn btn-primary">Lista użytkowników</a>
							</div>
									
						</div>
					</div>
				</div>
			</div>
		</div>
		</security:authorize>
		
		<security:authorize access="hasRole('ROLE_USER')">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="panel panel-default">
						<div class="panel-heading text-center">Menu</div>
						<div class="panel-body text-center">
							<div class="form-group">
								<a href="<spring:url value="/user/${loggedPersonName }"></spring:url>" class="btn btn-primary">Dane użytkownika</a>
							</div>
							<div>
								<a href="<spring:url value="/order/newOrder"></spring:url>" class="btn btn-primary">Otwórz koszyk</a>
							</div>
									
						</div>
					</div>
				</div>
			</div>
		</div>
		</security:authorize>
		
	</body>
</html>
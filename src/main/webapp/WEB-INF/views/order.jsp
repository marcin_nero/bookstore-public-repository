<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Zamówienie</title>
</head>
<body>

	<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
	
	<c:choose>
		<c:when test="${loggedPersonName=='anonymousUser'}">
			Musisz się zalogować, aby utworzyć koszyk zakupów
		</c:when>
		<c:otherwise>
			<div class="container">
				<div class="row">
					<div class="col-md-2"></div>
					
					<div class="col-md-8">
						<table class="table">
							<thead>
								<tr>
									<th>Tytuł</th>
									<th>Autor</th>
									<th>Ilość</th>
									<th>Cena</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${orderList }" var="book" >
									<tr>
										<td>${book.title} </td>
										<td>${book.author} </td>
										<td>1 </td>
										<td><fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2" /> zł </td>
										<td>
											<a href="<spring:url value="/order/delete?book_id=${book.id}"></spring:url>" class="btn btn-primary btn-large">
			    								<span class="glyphicon glyphicon-remove"></span>
			    								Usuń
			    							</a>
										</td>
									</tr>
								</c:forEach>
								<tr>
									<td> </td>
									<td> </td>
									<td>Suma </td>
									<td><fmt:formatNumber value="${total}" type="number" minFractionDigits="2" maxFractionDigits="2"  /> zł </td>
									<td></td>
								</tr>
							</tbody>
						</table>			
					</div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<a href="<spring:url value="cancelOrder?order_id=${order_id}"></spring:url>" class="btn btn-primary btn-large">
			    				<span class="glyphicon glyphicon-remove"></span>
			    				Anuluj zamówienie
			    			</a>
						</div>
						<div class="col-md-3">
							<a href="<spring:url value="finalizeOrder?order_id=${order_id }"></spring:url>" class="btn btn-primary btn-large">
			    				<span class="glyphicon glyphicon-check"></span>
			    			 	Złóż zamówienie
			    			</a>
						</div>
						<div class="col-md-3"></div>
					</div>
			</div>
		</c:otherwise>
	</c:choose>

</body>
</html>
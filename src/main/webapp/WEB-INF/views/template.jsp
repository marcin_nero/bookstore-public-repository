<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style>
		    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
		    .navbar {
		      margin-bottom: 10px;
		      border-radius:0%;
		    }
		    
		    /* Remove the jumbotron's default bottom margin */ 
		     .jumbotron {
		      margin-bottom: 0;
		    }
		   
		    /* Add a gray background color and some padding to the footer */
		    footer {
		      background-color: #f2f2f2;
		      padding: 25px;
		    }
		    
		    .carousel-inner img {
     			/*width: 100%;  Set width to 100% */
      			margin: auto;
      			min-height:200px;
  			}

		  /* Hide the carousel text when the screen is less than 600 pixels wide */
		  @media (max-width: 600px) {
		    .carousel-caption {
		      display: none; 
		    }
		  }
		</style>
</head>
<body>
	<div class="jumbotron">
	  <div class="container text-center">
	    <h1>Księgarnia internetowa YourBooks.com</h1>      
	    <p>Twoje książki w naszym sklepie</p>
	  </div>
	</div>
		
		<!-- Nawigacja strony -->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="<spring:url value="/"></spring:url>">Home</a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown">Kategorie<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<spring:url value="/byCategory/fantasy"></spring:url>">Fantasy</a></li>
								<li><a href="<spring:url value="/byCategory/kryminał"></spring:url>">Kryminał</a></li>
								<li><a href="<spring:url value="/byCategory/action"></spring:url>">Akcja</a></li>
								<li><a href="<spring:url value="/byCategory/kuchnia"></spring:url>">Kulinarne</a></li>
							</ul>
						</li>
						<li><a href="<spring:url value="/special/newInStore"></spring:url>">Nowości</a></li>
						<li><a href="<spring:url value="/special/bestseller"></spring:url>">Bestsellery</a></li>
						<li><a href="<spring:url value="/special/comingSoon"></spring:url>">Zapowiedzi</a></li>
						<li><a href="<spring:url value="/special/outlet"></spring:url>">Wyprzedaż</a></li>
						<li>
							<form method="get" action="<spring:url value="/search_"></spring:url>" >
								<fieldset>
									<input type="text" name="query"> <input type="submit" value="szukaj">			
								</fieldset>
							</form>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
					
						<c:choose>
							<c:when test="${loggedPersonName=='anonymousUser'}">
							</c:when>
							<c:otherwise>
								<li><a href="<spring:url value="/menu"></spring:url>"><span class="glyphicon glyphicon-log-in"></span>Menu</a></li>
							</c:otherwise>
						</c:choose>
						
						<c:choose>
							<c:when test="${loggedPersonName=='anonymousUser'}">
								<li><a href="<spring:url value="/login/"></spring:url>"><span class="glyphicon glyphicon-log-in"></span> Zaloguj</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="<spring:url value="/j_spring_security_logout"></spring:url>"><span class="glyphicon glyphicon-log-out"></span> Wyloguj(${loggedPersonName})</a></li>
							</c:otherwise>
						</c:choose>		
						
						<c:choose>
							<c:when test="${order == null }">
								<li><a href="<spring:url value="/order/newOrder"></spring:url>"><span class="glyphicon glyphicon-shopping-cart"></span>Koszyk</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="<spring:url value="/order/order?order_id=${order}"></spring:url>"><span class="glyphicon glyphicon-shopping-cart"></span>Koszyk</a></li>
							</c:otherwise>
						</c:choose>
						
						
					</ul>
				</div>
			</div>
		</nav>

</body>
</html>
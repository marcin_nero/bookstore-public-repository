<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
	
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="well text-center">
				${success}
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	
</body>
</html>
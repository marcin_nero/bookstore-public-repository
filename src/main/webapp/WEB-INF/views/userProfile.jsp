<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>User Profile</title> 	
 	</head>
	<body>
	
		<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>		

    	<section class="container">
    		<div class="row">
    			<div class="col-md-6 col-md-offset-3" >
    				<div class="thumbnail">
    					<div class="card"> 
	    					<div class="card-body">
		    					<h1 class="alert-success text-center">${user.login}</h1>
		    					<p>Imie: ${user.name}</p>
		    					<p>Nazwisko: ${user.lastName}</p>
		    					<p>Email: ${user.email}</p>
		    					<div class="form-group text-center">
									<a href="<spring:url value="/user/update/${loggedPersonName }"></spring:url>" class="btn btn-primary">Zaktualizuj dane</a>
								</div>
		    				</div>	
	    				</div>	
    				</div>
    			</div>
    		</div>
    	</section>
	</body>
</html>
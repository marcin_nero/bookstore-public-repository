<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<title>
	<spring:message code="siteTitle" />
</title>
</head>
<body>
	
	<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>

	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			
			<div class="col-md-6">
				<form:form modelAttribute="updateUser" class="form-horizontal">
					<form:errors path="*" cssClass="alert alert-danger" element="div" />
					<fieldset>
						<div class="panel panel-default">
							<div class="panel-header text-center">
								Aktualizacja danych użytkownika <span class="label label-default">${updateUser.getName()} ${updateUser.getLastName()}</span>
							</div>
							
							<div class="panel-body">
								<div class="form-group">
									<div class="row">
										<div class="col-md-3">
											Imię:
										</div>
										<div class="col-md-9">
											<form:input id="name" path="name" type="text" class="form:input-large"/>
											<form:errors path="name" cssClass="class text-danger" />
										</div>
									</div>
								</div>
							</div>
							
							<div class="panel-body">
								<div class="form-group">
									<div class="row">
										<div class="col-md-3">
											Nazwisko:
										</div>
										<div class="col-md-9">
											<form:input id="lastName" path="lastName" type="text" class="form:input-large"/>
											<form:errors path="lastName" cssClass="class text-danger" />
										</div>
									</div>
								</div>
							</div>
							
							<div class="panel-body">
								<div class="form-group">
									<div class="row">
										<div class="col-md-3">
											Email:
										</div>
										<div class="col-md-9">
											<form:input id="email" path="email" type="text" class="form:input-large"/>
											<form:errors path="email" cssClass="class text-danger" />
										</div>
									</div>
								</div>
							</div>
							
							<div class="panel-footer text-center">
								<div class="form-group">
			    					<div class="col-lg-offset-2 col-lg-10">
			    						<input type="submit" id="btnAdd" class="btn btn-primary" value="Zapisz">
			    					</div>
		    					</div>
							</div>
							
						</div>
					</fieldset>
				</form:form>
			</div>
			
			<div class="col-md-3"></div>
		</div>
	</div>
	
	
</body>
</html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<title><spring:message code="siteTitle" /></title>
		<style>
			div.panel-heading{
				white-space: nowrap; 
				overflow:hidden; 
				text-overflow:ellipsis;"
			}
			div.panel-heading:hover{
				overflow:visible;
			}
		</style>
	</head>
	<body>
    	
	<jsp:include page="/WEB-INF/views/template.jsp"></jsp:include>
		
	 <!-- karuzela fantasy -->
	 <div class="panel panel-info">
	 	<div class="panel-heading text-center">Fantasy</div>
	 	<div id="fantasyCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
			 	
			 	<!-- slidy -->
			 	
			 	<div class="carousel-inner">
			 		<div class="item active">
			 			<div class="container">
							<div class="row" >
								<div class="col-sm-1"></div>
								<c:forEach items="${fantasy }" var="book" begin="0" end="2">
									<div class="col-sm-3" >
										<div class="panel panel-default">
											<div class="panel-heading">${book.title }</div>
											<div class="panel-body">
												<img src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" class="img-responsive" alt="${book.title }" style="width:100%;height:300px"  >
											</div>
											<div class="panel-footer">
												<h5 class="text-danger">Cena: <fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2" /> zł</h5>
									    		<p>Autor: ${book.author}</p>
									    		<p>
									    			<a href=" <spring:url value="/book?id=${book.id }" />" class="btn btn-primary">
									    			Szczegóły
									    			</a>
									    		</p>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
			 		</div><!-- site 1 -->
			 		
			 		<div class="item">
			 			<div class="container">
							<div class="row">
								<div class="col-sm-1"></div>
								<c:forEach items="${fantasy }" var="book" begin="3" end="5">
									<div class="col-sm-3">
										<div class="panel panel-default">
											<div class="panel-heading" >${book.title }</div>
											<div class="panel-body">
												<img src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" class="img-responsive" alt="${book.title }" style="width:100%;height:300px"  >
											</div>
											<div class="panel-footer">
												<h5 class="text-danger">Cena: <fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2" /> zł</h5>
									    		<p>Autor: ${book.author}</p>
									    		<p>
									    			<a href=" <spring:url value="/book?id=${book.id }" />" class="btn btn-primary">
									    			Szczegóły
									    			</a>
									    		</p>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
			 		</div><!-- site 2 -->
			 		
			 		<div class="item">
			 			<div class="container">
							<div class="row">
								<div class="col-sm-1"></div>
								<c:forEach items="${fantasy }" var="book" begin="6" end="8">
									<div class="col-sm-3">
										<div class="panel panel-default">
											<div class="panel-heading " >${book.title }</div>
											<div class="panel-body">
												<img src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" class="img-responsive" alt="${book.title }" style="width:100%;height:300px"  >
											</div>
											<div class="panel-footer">
												<h5 class="text-danger">Cena: <fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2" /> zł</h5>
									    		<p>Autor: ${book.author}</p>
									    		<p>
									    			<a href=" <spring:url value="/book?id=${book.id }" />" class="btn btn-primary">
									    			Szczegóły
									    			</a>
									    		</p>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
			 		</div><!-- site 3 -->
			 		
			 	</div>
			 	
			 	<div>
			 	</div>
			 	
			 	<!-- Left and right controls -->
		 		<a class="left carousel-control" href="#fantasyCarousel" data-slide="prev">
		   			<span class="glyphicon glyphicon-chevron-left"></span>
		  			<span class="sr-only">Previous</span>
		  		</a>
		  		<a class="right carousel-control" href="#fantasyCarousel" data-slide="next">
		    		<span class="glyphicon glyphicon-chevron-right"></span>
		    		<span class="sr-only">Next</span>
		  		</a>
			 </div><!-- end of carousel -->
		 	</div>
		 	
		 	 <!-- karuzela action -->
			 <div class="panel panel-info">
			 	<div class="panel-heading text-center">Akcja</div>
			 	<div id="actionCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
					 	<!-- kropki -->
					 	<ol class="carousel-indicators">
					 		<li data-target="#actionCarousel" data-slide-to="0" class="active">
					 		<li data-target="#actionCarousel" data-slide-to="1">
					 	</ol>
					 	
					 	<!-- slidy -->
					 	
					 	<div class="carousel-inner">
					 		<div class="item active">
					 			<div class="container">
									<div class="row">
										<div class="col-sm-1"></div>
										<c:forEach items="${action }" var="book" begin="0" end="2">
											<div class="col-sm-3">
												<div class="panel panel-default">
													<div class="panel-heading">${book.title }</div>
													<div class="panel-body">
														<img src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" class="img-responsive" alt="${book.title }" style="width:100%;height:300px"  >
													</div>
													<div class="panel-footer">
														<h5 class="text-danger">Cena: <fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2" /> zł</h5>
											    		<p>Autor: ${book.author}</p>
											    		<p>
											    			<a href=" <spring:url value="/book?id=${book.id }" />" class="btn btn-primary">
											    			Szczegóły
											    			</a>
											    		</p>
													</div>
												</div>
											</div>
										</c:forEach>
									</div>
								</div>
					 		</div><!-- site 1 -->
					 		
					 		<div class="item">
					 			<div class="container">
									<div class="row">
										<c:forEach items="${action }" var="book" begin="3" end="5">
											<div class="col-sm-3">
												<div class="panel panel-default">
													<div class="panel-heading">${book.title }</div>
													<div class="panel-body">
														<img src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" class="img-responsive" alt="${book.title }" style="width:100%;height:300px"  >
													</div>
													<div class="panel-footer">
														<h5 class="text-danger">Cena: <fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2" /> zł</h5>
											    		<p>Autor: ${book.author}</p>
											    		<p>
											    			<a href=" <spring:url value="/book?id=${book.id }" />" class="btn btn-primary">
											    			Szczegóły
											    			</a>
											    		</p>
													</div>
												</div>
											</div>
										</c:forEach>
									</div>
								</div>
					 		</div><!-- site 2 -->
					 		
					 		<div class="item">
					 			<div class="container">
									<div class="row">
										<c:forEach items="${action }" var="book" begin="6" end="8">
											<div class="col-sm-3">
												<div class="panel panel-default">
													<div class="panel-heading">${book.title }</div>
													<div class="panel-body">
														<img src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" class="img-responsive" alt="${book.title }" style="width:100%;height:300px"  >
													</div>
													<div class="panel-footer">
														<h5 class="text-danger">Cena: <fmt:formatNumber value="${book.price}" type="number" minFractionDigits="2" maxFractionDigits="2" /> zł</h5>
											    		<p>Autor: ${book.author}</p>
											    		<p>
											    			<a href=" <spring:url value="/book?id=${book.id }" />" class="btn btn-primary">
											    			Szczegóły
											    			</a>
											    		</p>
													</div>
												</div>
											</div>
										</c:forEach>
									</div>
								</div>
					 		</div><!-- site 3 -->
					 		
					 	</div>
					 	
					 	<!-- Left and right controls -->
				 		<a class="left carousel-control" href="#actionCarousel" data-slide="prev">
				   			<span class="glyphicon glyphicon-chevron-left"></span>
				  			<span class="sr-only">Previous</span>
				  		</a>
				  		<a class="right carousel-control" href="#actionCarousel" data-slide="next">
				    		<span class="glyphicon glyphicon-chevron-right"></span>
				    		<span class="sr-only">Next</span>
				  		</a>
					 </div><!-- end of carousel -->
				 	</div>
		 	
		 	
	</body>
</html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
 	</head>
 	<body>
    	<div class="jumbotron alert-info">
    		<div class="container">
    			<a href="<spring:url value="/books/" />">
    				<img src="<c:url value="/resource/images/logo.png"> </c:url>" alt="logo">
    			</a>
    		</div>
    	</div>
    	<section class="container">
    		<div class="row">
	    		<div class="list-group col-sm-1 text-left">	
	    			<table class="table table-sm table-hover">
	    				<thead>
	    					<tr>
	    						<th>
									<a class="btn btn-primary btn-block disabled">Menu</a>	    						
	    						</th>
	    					</tr>
	    				</thead>
	    				<tbody>
	    					<tr>
	    						<td>
	    							<a class="btn btn-outline-primary btn-block" href="<spring:url value="/books/byCategory/fantasy" />">Fantasy</a>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							<a class="btn btn-outline-primary btn-block" href="<spring:url value="/books/byCategory/action" />">Akcja</a>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							<a class="btn btn-outline-primary btn-block" href="<spring:url value="/books/byCategory/kryminał" />">Kryminał</a>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td>
	    							<a class="btn btn-outline-primary btn-block" href="<spring:url value="/books/byCategory/kuchnia" />">Kuchnia</a>
	    						</td>
	    					</tr>
	    				</tbody>
	    			</table>
	    		</div>
	    		<div class="body col-sm-11 ">	
		    		<div class="row">
		    			<c:forEach items="${books }" var="book">
			    			<div class="col-sm-2">
			    				<div class="thumbnail">
			    					<div class="card" > 
			    						<img class="card-img-top" src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" alt="${book.title }" style="width:100%; height:250px"  >
				    					<div class="card-block">
				    						<h4 class="card-title" style="white-space:nowrap; overflow:hidden; text-overflow: ellipsis">${book.title }</h4>
					    					<h5 class="text-danger">Cena: <strong>${book.price} zł</strong></h3>
					    					<p>Autor: ${book.author}</p>
					    					<p>
					    						<a href=" <spring:url value="/books/book?id=${book.id }" />" class="btn btn-primary">
					    							Szczegóły
					    						</a>
					    					</p>
					    				</div>	
				    				</div>	
			    				</div>
			    			</div>
			    		</c:forEach>
		    		</div>	
		    	</div>
	    	</div>
    	</section>
 
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  </body>
</html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
 	</head>
 	<body>
    	<div class="jumbotron alert-info">
    		<div class="container">
    			<h1 class="text-center">${greeting}</h1>
    		</div>
    	</div>
    	<section class="container">
    		<div class="row">
    			<div class="col-md-5">
    				<img src="<c:url value="/resource/images/${book.id}.jpg"> </c:url>" alt="${book.title }" style="width:100%"  >
    			</div>
    			<div class="col-md-5">
    				<h1 class="alert-success text-center">${book.title}</h1>
			    	<h3 class="text-danger">Cena: ${book.price} zł</h3>
			    	<p>Autor: ${book.author}</p>
			    	<p>Kategoria: ${book.category}</p>
			    	<p>Opis: ${book.review}</p>
			    	<p>Liczba sztuk w magazynie: ${book.unitsInStock }</p>
			    	<p>
			    		<strong>Kod produktu: </strong><span class="badge badge-primary">${book.id }</span>  
			    	</p>
			    	<p>
			    		<a href="<spring:url value="/books/" />" class="btn btn-default">
			    			<span class="glyphicons glyphicons-undo">Wstecz</span>
			    		</a>
			    		<a href="#" class="btn btn-warning btn-large">
			    			<span class="glyphicon-shopping-cart glyphicon"></span>
			    			Zamów teraz
			    		</a>
			    	</p>
    			</div>
    		</div>
    	</section>
 		
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  </body>
</html>
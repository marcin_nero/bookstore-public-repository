<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style>
		    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
		    .navbar {
		      margin-bottom: 0px;
		      border-radius:0%;
		    }
		    
		    /* Remove the jumbotron's default bottom margin */ 
		     .jumbotron {
		      margin-bottom: 0;
		    }
		   
		    /* Add a gray background color and some padding to the footer */
		    footer {
		      background-color: #f2f2f2;
		      padding: 25px;
		    }
		    
		    .carousel-inner img {
     			/*width: 100%;  Set width to 100% */
      			margin: auto;
      			min-height:200px;
  			}

		  /* Hide the carousel text when the screen is less than 600 pixels wide */
		  @media (max-width: 600px) {
		    .carousel-caption {
		      display: none; 
		    }
		  }
		</style>
		<title><spring:message code="siteTitle" /></title>
	</head>
	<body>
		<div class="jumbotron">
		  <div class="container text-center">
		    <h1>Księgarnia internetowa YourBooks.com</h1>      
		    <p>Twoje książki w naszym sklepie</p>
		  </div>
		</div>
		
		<!-- Nawigacja strony -->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="<spring:url value="/home/"></spring:url>">Home</a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown">Kategorie<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<spring:url value="/books/byCategory/fantasy"></spring:url>">Fantasy</a></li>
								<li><a href="<spring:url value="/books/byCategory/kryminał"></spring:url>">Kryminał</a></li>
								<li><a href="<spring:url value="/books/byCategory/action"></spring:url>">Akcja</a></li>
								<li><a href="<spring:url value="/books/byCategory/kuchnia"></spring:url>">Gotowanie</a></li>
							</ul>
						</li>
						<li><a href="<spring:url value="#"></spring:url>">Nowości</a></li>
						<li><a href="<spring:url value="#"></spring:url>">Bestsellery</a></li>
						<li><a href="<spring:url value="#"></spring:url>">Zapowiedzi</a></li>
						<li><a href="<spring:url value="#"></spring:url>">Wyprzedaż</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="<spring:url value="/login/"></spring:url>"><span class="glyphicon glyphicon-user"></span>Login</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>Koszyk</a></li>
					</ul>
				</div>
			</div>
		</nav>
	
				
	
		
	</body>
</html>
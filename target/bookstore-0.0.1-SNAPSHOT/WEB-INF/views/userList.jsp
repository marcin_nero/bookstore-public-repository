<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<title>Bookstore Users List</title> 	
 	</head>
	<body>
		<div class="jumbotron alert-info">
    		<div class="container">
    			<h1 class="text-center">${greeting}</h1>
    		</div>
    	</div>
    	<section class="container">
	    	<table class="table table-dark table-hover">
	    		<thead>
	    			<tr>
	    				<th>Login</th>
	    				<th>Imię</th>
	    				<th>Nazwisko</th>
	    				<th>Email</th>
	    			</tr>
	    		</thead>
    			<tbody>
	    			<c:forEach items="${users }" var="user">	    				
		    			<tr onclick="window.location.href='/bookstore/user/${user.login}'" style="cursor.pointer">
		    					<td>${user.login }</td>
		    					<td>${user.name }</td>
		    					<td>${user.lastName }</td>
		    					<td>${user.email }</td>
		    			</tr>
	    			</c:forEach>
	    		</tbody>	
	    	</table>
    	</section>
	</body>
</html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
 		<title>Książki</title>
 	</head>
 	<body>
	 	<div class="jumbotron alert-info">
	 		<a href="<c:url value="/j_spring_security_logout" /> " class="btn btn-danger btn-mini pull-right">
	 			Wyloguj się
	 		</a>
	    	<div class="container">
	    		<h1 class="text-center">${greeting}</h1>
	    	</div>
	    </div>
	    <section class="container">
	    	<div class="row">
	    		<div class="col-md-8">
	    			<form:form modelAttribute="newBook" class="form-horizontal">
	    				<fieldset>
	    					<legend>Dodaj nowy produkt</legend>
	    					
	    					<p>Uwaga! pola oznaczone * są wymagane do wprowadzenia nowego produktu do bazy danych</p>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="category">Kategoria *</label>
	    						<div class="col-lg-10">
	    							<form:input id="category" path="category" type="text" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="title">Tytuł *</label>
	    						<div class="col-lg-10">
	    							<form:input id="title" path="title" type="text" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="author">Autor *</label>
	    						<div class="col-lg-10">
	    							<form:input id="author" path="author" type="text" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="id">
									<spring:message code="addBook.form.id.label" />
								</label>
	    						<div class="col-lg-10">
	    							<form:input id="id" path="id" type="text" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="price">Cena *</label>
	    						<div class="col-lg-10">
	    							<form:input id="price" path="price" type="number" min="1.00" max="" step="0.01" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="review">Opis</label>
	    						<div class="col-lg-10">
	    							<form:input id="review" path="review" type="text" maxlength="250" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="publisher">Wydawca</label>
	    						<div class="col-lg-10">
	    							<form:input id="publisher" path="publisher" type="text" class="form:input-large"/>
	    						</div>
	    					</div>
	    				
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="language">Język</label>
	    						<div class="col-lg-10">
	    							<form:input id="language" path="language" type="text" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="ISBN">ISBN</label>
	    						<div class="col-lg-10">
	    							<form:input id="ISBN" path="ISBN" type="text" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<label class="control-label col-lg-4" for="unitsInStock">Ilość</label>
	    						<div class="col-lg-10">
	    							<form:input id="unitsInStock" path="unitsInStock" type="number" min="1" max="100" step="1" class="form:input-large"/>
	    						</div>
	    					</div>
	    					
	    					<div class="form-group">
	    						<div class="col-lg-offset-2 col-lg-10">
	    							<input type="submit" id="btnAdd" class="btn btn-primary" value="Dodaj">
	    						</div>
	    					</div>
	    				</fieldset>
	    			</form:form>
	    		</div>
	    	</div>
	    </section>
 	
 		<!-- Optional JavaScript -->
	    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
 	</body>
</html>
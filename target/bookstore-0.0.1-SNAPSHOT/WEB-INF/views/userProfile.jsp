<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<title>User Profile</title> 	
 	</head>
	<body>
		<div class="jumbotron alert-info">
    		<div class="container">
    			<h1 class="text-center">${greeting}</h1>
    		</div>
    	</div>
    	<section class="container">
    		<div class="row">
    			<div class="col-md-6 col-md-offset-3" >
    				<div class="thumbnail">
    					<div class="card"> 
	    					<div class="card-body">
		    					<h1 class="alert-success text-center">${user.login}</h1>
		    					<p>Imie: ${user.name}</p>
		    					<p>Nazwisko: ${user.lastName}</p>
		    					<p>Email: ${user.email}</p>
		    				</div>	
	    				</div>	
    				</div>
    			</div>
    		</div>
    	</section>
	</body>
</html>